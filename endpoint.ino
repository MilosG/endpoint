//Libraries
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <HTTPClient.h>


//variables
const char* ssid = "ssid"; //SSID
const char* password = "password"; //Password
const int light1 = 7; //LIGHT PIN     
const int endpointType = 1;
const int btn1 = 13;
int endpointState = 1; //1 - ON, 2 - OFF, 3 - AUTO
int buttonState = 0;

String apiName = "https://localhost:44341/api/EndPointCommunication";
String macAddress = "";

     
void setup(){	 	
  	pinMode(A0, INPUT);
  	pinMode(light1, OUTPUT);
  	pinMode(btn1, INPUT);
   	Serial.begin(9600);
   	
    WiFi.begin(ssid,password);
  	while (WiFi.status() != WL_CONNECTED) 
  	{
     	delay(500);
    	Serial.print("connecting");
  	}
    Serial.print("successfully connected");  
   	macAddress = mac2String(WiFi.macAddress()); 
  	String url = apiName+"/RegisterDevice/"+macAddress+"/"+endpointType;
  	http.begin(url);
  	int httpCode = http.GET();                                                                  //Send the request  	
 	http.end();
    
}
void loop()
{
  buttonState = digitalRead(btn1);
  if(buttonState == HIGH){
    endpointState = ((endpointState + 1)%3) + 1; 
  }
  //ON
  if(endpointState == 1)
  {
    lightOn();        
  }
  //OFF
  else if(endpointState == 2)
  {
    lightOff();        
  }
  //AUTOMATIC
  else if(endpointState == 3)
  {        
  	
    delay(30000);
    int senzorValue = analogRead(A0);  	
   	String url = apiName+"/NewValue/"+macAddress+"/"+senzorValue;

 	http.begin(url);
  	int httpCode = http.GET();                                                                  //Send the request
 	if (httpCode > 0){
  		String response = http.getString();
      	if(response == "1")
      	{
          	lightOn();      		
        } else {
        	lightOff();
        }          
    }
  	http.end();
  }  	
}
void lightOn()
{
  digitalWrite(light1,HIGH); 
}

void lightOff()
{
  digitalWrite(light1,LOW);      
}


String mac2String(byte ar[]){
  	String s;
  	for (byte i = 0; i < 6; ++i)
  	{
    	char buf[3];
    	sprintf(buf, "%2X", ar[i]);
    	s += buf;
    	if (i < 5) s += ':';
  	}
	return s;
}
